import matplotlib.pylab as plt

colors=['blue','green','pink','cyan','magenta','yellow','black','red','gray','gold']

def show_2d_data(path):
    x1=list()
    x2=list()
    y=list()
    with open(path) as f:
        for line in f:
            arr=line.strip().split(",")
            x1.append(float(arr[0]))
            x2.append(float(arr[1]))
            y.append(int(arr[2]))
    for a,b,c in zip(x1,x2,y):
        plt.scatter([a],[b],c=colors[c])
    plt.show()

if __name__=="__main__":
    show_2d_data('2d_data_nc_2_circle.txt')
    show_2d_data('2d_data_nc_2.txt')
    show_2d_data('2d_data_nc_3.txt')
    show_2d_data('2d_data_nc_4.txt')
    show_2d_data('2d_data_nc_5.txt')