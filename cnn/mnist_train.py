from __future__ import print_function
import argparse
import torch
import torch.nn
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.cuda
import torch.utils.data
import os

import numpy as np

from mnist_data import MnistDataset
from mlp.functions import soft_maxout

        
class Net(nn.Module):
    
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=3)
        self.act1 = nn.ReLU()
        self.pool1 = nn.MaxPool2d(2,2)
        
        self.conv2 = nn.Conv2d(10, 20, kernel_size=3)
        self.act2 = nn.ReLU()
        self.pool2 = nn.MaxPool2d(2,2)
        
        self.conv3 = nn.Conv2d(20, 20, kernel_size=3)
        self.act3 = nn.ReLU()
        
        self.fc=nn.Linear(180,10)

    def forward(self, x):
        x = self.conv1(x)
        x = self.act1(x)
        x = self.pool1(x)
        
        x = self.conv2(x)
        x = self.act2(x)
        x = self.pool2(x)
        
        x = self.conv3(x)
        x = self.act3(x)
        x = x.view(-1, 180)
        
        x=self.fc(x)
        
        return x

class Net_S(nn.Module):
    
    def __init__(self):
        super(Net_S, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, kernel_size=3)
        self.act1 = soft_maxout
        self.pool1 = nn.MaxPool2d(2,2)
        
        self.conv2 = nn.Conv2d(10, 20, kernel_size=3)
        self.act2 = soft_maxout
        self.pool2 = nn.MaxPool2d(2,2)
        
        self.conv3 = nn.Conv2d(10, 20, kernel_size=3)
        self.act3 = soft_maxout
        
        self.fc=nn.Linear(90,10)

    def forward(self, x):
        x = self.conv1(x)
        x = self.act1(x)
        x = self.pool1(x)
        
        x = self.conv2(x)
        x = self.act2(x)
        x = self.pool2(x)
        
        x = self.conv3(x)
        x = self.act3(x)
        
        x = x.reshape([-1, 90])
        
        x=self.fc(x)
        
        return x
    
def train(net,data_path,model_path,batch_size=128):
    
    train_data = MnistDataset(data_path,train=True,data_dim=3,binary_data=True)
    train_data_loader = torch.utils.data.DataLoader(train_data,batch_size=batch_size,shuffle=True)

    val_data = MnistDataset(data_path,train=False,data_dim=3,binary_data=True)
    val_data_loader = torch.utils.data.DataLoader(val_data,batch_size=batch_size,shuffle=False)

    net_model=net
    if os.path.exists(model_path):
        print('load model:',model_path)
        sd=torch.load(model_path)
        net_model.load_state_dict(sd)
    
    is_cuda = torch.cuda.is_available()
    cost = torch.nn.CrossEntropyLoss()

    if is_cuda:
        net_model = net_model.cuda()
        cost = cost.cuda()
    
    optimizer = optim.SGD(net_model.parameters(), lr=0.01, momentum=0.9)
    
    for step in range(100):
        net_model.train()
        
        for batch_index,datalabel in enumerate(train_data_loader):
            data,target = datalabel
            if is_cuda:
                data, target = data.cuda(), target.cuda()
            data, target = Variable(data), Variable(target)
            optimizer.zero_grad()
            output = net_model(data)
            pred = output.data.max(1)[1]
            correct = pred.eq(target.data).cpu().sum().numpy()
            loss = cost(output, target)
            loss.backward()
            optimizer.step()
            if batch_index % 40 == 0:
                print(str(step),"/",batch_index,'train loss: {:.4f}, accuracy: {}/{} ({:.3f}%)'.format(loss.item(),correct,batch_size,100.*correct/batch_size))
                
        net_model.eval()
        with torch.no_grad():
            test_loss = 0
            correct = 0
            num_data = 0
            for batch_index,datalabel in enumerate(val_data_loader):
                data,target = datalabel
                if is_cuda:
                    data, target = data.cuda(), target.cuda()
                data, target = Variable(data), Variable(target)
                output = net_model(data)
                test_loss += cost(output, target).item()
                pred = output.data.max(1)[1]
                correct += pred.eq(target.data).cpu().sum().numpy()
                num_data += len(data)

            print('val loss: {:.4f}, accuracy: {}/{} ({:.3f}%)\n'.format(test_loss/len(val_data_loader),correct,num_data,100.*correct/num_data))
        
        torch.save(net_model.state_dict(),model_path)
        print("save model:",model_path)

if __name__=="__main__":
    train(Net(),'../dataset/mnist.pkl.gz',"../temp/minist.pth")    
    #train(Net_S(),'../dataset/mnist.pkl.gz',"../temp/minist_s.pth")    
    

