import numpy as np
import gzip
import pickle
import torch
from torchvision import  transforms
from torch.utils.data import Dataset,DataLoader

class MnistDataset(Dataset):

    def __init__(self,mnist_path,train=True,data_dim=3,binary_data = True):
        super(Dataset,self).__init__()
        self.mnist_path=mnist_path
        self.load_minist(mnist_path)
        if train:
            self.data = list(zip(self.train_x,self.train_y))
        else:
            self.data = list(zip(self.valid_x,self.valid_y))
        self.data_dim = data_dim
        self.binary_data = binary_data

    def __len__(self) -> int:
        return len(self.data)
    
    def __getitem__(self, index: int):

        data,label = self.data[index]
        if self.data_dim==3:
            data=np.reshape(data, [1,28,28])
        elif self.data_dim==2:
            data=np.reshape(data, [28,28])

        if self.binary_data:
            data = (data>0.2).astype(np.float32)

        return data,label

    def load_minist(self,dataset):
        print("load dataset")
        f = gzip.open(dataset, 'rb')
        train_set, valid_set, test_set = pickle.load(f,encoding='latin1')
        f.close()
    
        self.train_x,self.train_y=train_set
        self.valid_x,self.valid_y=valid_set
        self.test_x , self.test_y=test_set
        print("train image,label shape:",self.train_x.shape,self.train_y.shape)
        print("valid image,label shape:",self.valid_x.shape,self.valid_y.shape)
        print("test  image,label shape:",self.test_x.shape,self.test_y.shape)
        print("load dataset end")

if __name__=="__main__":
    import matplotlib.pylab as plt
    md = MnistDataset('../temp/mnist.pkl.gz',data_dim=2,binary_data=True)
    data_loader = DataLoader(md,batch_size=64,shuffle=False)
    for i,datalabel in enumerate(data_loader):
        data,label = datalabel
        print(data.shape,label.shape)
        for d,l in zip(data,label):
            im = d.data.numpy()
            print(l)
            plt.imshow(im)
            plt.show()

    