import sys
sys.path.insert(0,"../")
import torch
import numpy as np
import matplotlib.pylab as plt
from mlp.linear_regions import max_linear_regions

def format(x,size=9):
    num = size - len(x)
    y = x
    for i in range(num):
        y = '0'+y
    return y

def to_code(x,size):
    b = bin(x)
    b = b.split('b')[1]
    sc = format(b,size)
    return list(map(int,list(sc)))

print("max linear regions:",max_linear_regions(dim=4,m=10))
print("max linear regions:",max_linear_regions(dim=9,m=10))

input_dim = 9
num_probs = 2**input_dim
xis = np.zeros((num_probs,input_dim))
for n in range(num_probs):
    xis[n]=np.array(to_code(n,input_dim))

pm=torch.load("../temp/minist.pth")
for k,v in pm.items():
    print(k)

w1 = pm['conv1.weight']
b1 = pm['conv1.bias']

code = np.zeros((num_probs,10))
for n,x in enumerate(xis):
    index = 0
    for w,b in zip(w1,b1):
        out = np.dot(w.numpy().flatten(),x)+b.numpy()
        code[n][index]=0 if out<=0 else 1
        index+=1
    ec = "".join(list(map(str,code[n].astype(np.uint8))))
    if ec=="1111110111":
       plt.imshow(np.reshape(x,(3,3))*255)
       plt.show()
      
print(code)

regions_map = dict()
for c in code:
    ec = "".join(list(map(str,c.astype(np.uint8))))
    if regions_map.get(ec) is None:
        regions_map[ec]=0
    regions_map[ec]+=1

print("num regions:",len(regions_map))
for k,v in regions_map.items():
    print(k,v)
    
    
