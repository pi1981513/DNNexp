import torch
import torch.nn

def maxout(x:torch.Tensor, k_input=2):
    assert len(x.size())==2 or len(x.size())==4,"not support input shape:[%s]"%(','.join(map(str,x.size())))
    assert x.size()[1]%k_input==0,"x 2nd dim should n-times of %s"%k_input
    if len(x.size())==2:
        x = torch.reshape(x,(x.size()[0],-1,k_input))
        return torch.max(x,dim=-1)[0]
    elif len(x.size())==4:
        n,c,h,w = x.size()
        x = x.permute([0,2,3,1])
        x = torch.reshape(x,(n,h,w,-1,k_input))
        y = torch.max(x,dim=-1)[0]
        return y.permute([0,3,1,2])

def soft_maxout(x:torch.Tensor, k_input=2):
    assert len(x.size())==2 or len(x.size())==4,"not support input shape:[%s]"%(','.join(map(str,x.size())))
    assert x.size()[1]%k_input==0,"x 2nd dim should n-times of %s"%k_input
    if len(x.size())==2:
        x = torch.reshape(x,(x.size()[0],-1,k_input,1))
        xT = torch.transpose(x,2,3)
        y = torch.softmax(xT,dim=-1) @ x
        return y.view(y.size()[0],-1)  
    elif  len(x.size())==4:
        n,c,h,w = x.size()
        x = x.permute([0,2,3,1])
        x = torch.reshape(x,(n,h,w,-1,k_input,1))
        xT = torch.transpose(x,4,5)
        y = torch.softmax(xT,dim=-1) @ x
        y = y.view(n,h,w,-1)
        return y.permute([0,3,1,2])