import argparse
import torch
import torch.nn
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.cuda
import torch.utils.data
import os
import file as fu
import numpy as np

from data import DataManager

class MLP_2x2x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x2x2, self).__init__()
        self.fc1=nn.Linear(2,2)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(2,2)

    def forward(self, x):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        return h,x

class MLP_2x3x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x3x2, self).__init__()
        self.fc1=nn.Linear(2,3)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(3,2)

    def forward(self, x):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        return h,x

class MLP_2x5x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x5x2, self).__init__()
        self.fc1=nn.Linear(2,5)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(5,2)

    def forward(self, x):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        return h,x

class MLP_2x10x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x10x2, self).__init__()
        self.fc1=nn.Linear(2,10)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(10,2)

    def forward(self, x):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        return h,x
    
class MLP_2x3x3x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x3x3x2, self).__init__()
        self.fc1=nn.Linear(2,3)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(3,3)
        self.relu2=nn.ReLU()
        self.fc3=nn.Linear(3,2)

    def forward(self, x):
        x = self.fc1(x)
        h1 = self.relu1(x)
        x = self.fc2(h1)
        h2 = self.relu2(x)
        x = self.fc3(h2)
        return [h1,h2],x
    

def train(model:nn.Module,data_path,model_path=None):
    
    dm=DataManager(data_path)
   
    #model.cuda()
    cost = torch.nn.CrossEntropyLoss()#.cuda()
    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
    batch_size=200
    
    for k,v in model.named_parameters():
        print(k,v)
    
    for epoch in range(1000):
        model.train()
        
        for batch_index in range(10):
            data,label=dm.next_batch_train(batch_size=batch_size)
            data, target = torch.FloatTensor(data), torch.LongTensor(label)
            #data, target = data.cuda(), target.cuda()
            data, target = Variable(data), Variable(target)
            optimizer.zero_grad()
            _,output = model(data)
            pred = output.data.max(1)[1]
            correct = pred.eq(target.data).cpu().sum()
            loss = cost(output, target)
            loss.backward()
            optimizer.step()
            print(str(epoch*5+batch_index),'train loss: {:.4f}, accuracy: {}/{} ({:.3f}%)'.format(loss.data.item(),correct,batch_size,100.*correct/batch_size))

        if model_path is not None:   
            torch.save(model.state_dict(),model_path)
            print("save:",model_path)

def infer(data,model_path):
    if 'MLP_2x3x2' in model_path:
        model = MLP_2x3x2()
    elif 'MLP_2x5x2' in model_path:
        model = MLP_2x5x2()
    elif 'MLP_2x10x2' in model_path:
        model = MLP_2x10x2()
    elif 'MLP_2x3x3x2' in model_path:
        model = MLP_2x3x3x2()
    else:
        print("no model found.")
        return None,None,None
    assert os.path.exists(model_path),"%s not exist."%(model_path)
    
    sd=torch.load(model_path)
    model.load_state_dict(sd)
    model.eval()
    data = Variable(torch.FloatTensor(data))
    h,output = model(data)
    if isinstance(h,list):
        h = [x.data.cpu().numpy() for x in h]
    else:
        h = h.data.cpu().numpy()
    y = output.data.cpu().numpy()
    return h,y,np.argmax(y,axis=1)

def save_hidden(path="../dataset/2d_data_circle.txt"):
    dm=DataManager(path,shuffle=False)
    data,label=dm.data_all()
    hidden=infer(data)
    p="../dataset/2d_data_circle_hidden.txt"
    fu.write(p)
    for h,l in zip(hidden,label):
        fu.append(p,"%s,%s,%s,%s\n"%(h[0],h[1],h[2],l))
    
    return hidden,label
        
    

if __name__=="__main__":
    #train(MLP_2x2x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x2x2.pth")#not converge
    #train(MLP_2x3x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x3x2.pth") 
    #train(MLP_2x5x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x5x2.pth") 
    #train(MLP_2x10x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x10x2.pth")
    train(MLP_2x3x3x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x3x3x2.pth")  
       

