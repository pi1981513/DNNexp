import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import torch
from dataset.show_data import colors

def view_result_mlp2x3x2(data_path,model_path,img_save_path="",constraint_domain=False,show_hidden=True):
    '''
    constraint_domain: constraint fc layer input domain
    '''
    from train_mlp_relu import infer
    pm=torch.load(model_path)
    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    for k,v in pm.items():
        print(k,v)

    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    wr = w2[0] - w2[1]
    br = b2[0] - b2[1]
    print("wr hyperplane:",wr,br)

    #encode linear regions
    Eks = []
    for i in range(2):
        for j in range(2):
            for k in range(2):
                Eks.append([i,j,k])

    code_color_map = dict()
    #plot sub-region decision hyperplane
    for index,Ek in enumerate(Eks):
        Dk = np.diag(Ek)
        #print(Ek,wr,w0)
        w = np.dot(np.dot(wr.T,Dk),w1)
        b = np.dot(np.dot(wr.T,Dk),b1) + br
        print('hyperplane final:%s'%(','.join(map(str,Ek))),w,b,colors[index])

        x0=np.linspace(0.0,1.0,1000)
        x1=-(w[0]*x0+b)/w[1]

        input=list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                input.append([xx,yy])
        if len(input)==0:
            continue
        h,y,c = infer(input,model_path=model_path)
        codes = (h>0).astype(np.int32)

        #plot hyperplanes
        x,y=list(),list()
        for xy,code in zip(input,codes):
            if not constraint_domain:
                x.append(xy[0])
                y.append(xy[1])
            else:
                #constraint hyperplanes in sub-region
                if "".join(map(str,code))=="".join(map(str,Ek)):
                    x.append(xy[0])
                    y.append(xy[1])
        plt.subplot(2,2,1)
        plt.plot(x,y,c=colors[index])
        code_color_map["".join(map(str,Ek))]=colors[index]

    #plot hidden hyperplanes
    x0=np.linspace(0.0,1.0,1000)
    index=0
    for w,b in zip(w1,b1):
        print("hidden node:%s"%(index),w,b,colors[index])
        x1=-(w[0]*x0+b)/w[1]

        x,y=list(),list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                x.append(xx)
                y.append(yy)
        if show_hidden:
            plt.subplot(2,2,1)
            plt.plot(x,y,'--',c=colors[index])
        index+=1

    #plot data points
    data=dict()
    input=list()
    label=list()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
            input.append([float(arr[0]),float(arr[1])])
            label.append(l)

    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.subplot(2,2,1)
        plt.scatter(x,y,c=colors[k])

    #plot hidden out data and sub-region hyperplanes
    h,y,c = infer(input,model_path=model_path)
    codes = (h>0).astype(np.int32)

    for xy,code,cc in zip(h,codes,label):
        if "".join(map(str,code))=="011":
	    #print(xy)
            plt.subplot(2,2,2)
            plt.scatter(xy[1],xy[2],c=colors[cc])
            
        if "".join(map(str,code))=="101":
	    #print(xy)
            plt.subplot(2,2,3)
            plt.scatter(xy[0],xy[2],c=colors[cc])
            
        if "".join(map(str,code))=="110":
	    #print(xy)
            plt.subplot(2,2,4)
            plt.scatter(xy[0],xy[1],c=colors[cc])
            

    #sub-region "011"
    x=np.linspace(0.0,1.5,1000)
    y=-(wr[1]*x+br)/wr[2]
    plt.subplot(2,2,2)
    plt.plot(x,y,c=code_color_map.get('011'))
    plt.title("sub-region[011] hidden hyperplane")

    #sub-region "101" 
    y=-(wr[0]*x+br)/wr[2]
    plt.subplot(2,2,3)
    plt.plot(x,y,c=code_color_map.get('101'))
    plt.title("sub-region[101] hidden hyperplane")
    
    #sub-region "110"
    y=-(wr[0]*x+br)/wr[1]
    plt.subplot(2,2,4)
    plt.plot(x,y,c=code_color_map.get('110'))
    plt.title("sub-region:[110] hidden hyperplane")

    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

if __name__=="__main__":
    
    view_result_mlp2x3x2(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_2x3x2.pth",
              constraint_domain=True,
              show_hidden=True
              )
