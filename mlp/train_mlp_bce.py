import argparse
import torch
import torch.nn
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.cuda
import torch.utils.data
import os
import file as fu
import numpy as np

from data import DataManager

class MLP_2x3x1(nn.Module):
    
    def __init__(self):
        super(MLP_2x3x1, self).__init__()
        self.fc1=nn.Linear(2,3)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(3,1)

        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.Linear)):
                nn.init.orthogonal_(m.weight)

    def forward(self, x, test= False):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        if test:
            x = torch.sigmoid(x)
        return h,x
    
class MLP_2x5x1(nn.Module):
    
    def __init__(self):
        super(MLP_2x5x1, self).__init__()
        self.fc1=nn.Linear(2,5)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(5,1)

        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.Linear)):
                nn.init.orthogonal_(m.weight)

    def forward(self, x, test= False):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        if test:
            x = torch.sigmoid(x)
        return h,x
    
    
class MLP_2x10x1(nn.Module):
    
    def __init__(self):
        super(MLP_2x10x1, self).__init__()
        self.fc1=nn.Linear(2,10)
        self.relu1=nn.ReLU()
        self.fc2=nn.Linear(10,1)

        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.Linear)):
                nn.init.orthogonal_(m.weight)

    def forward(self, x, test= False):
        x = self.fc1(x)
        h = self.relu1(x)
        x = self.fc2(h)
        if test:
            x = torch.sigmoid(x)
        return h,x
    
def train(model:nn.Module,data_path,model_path=None):
    
    dm=DataManager(data_path)
   
    #model.cuda()
    cost = torch.nn.BCEWithLogitsLoss()#.cuda()
    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
    batch_size=200
    
    for k,v in model.named_parameters():
        print(k,v)
    
    for epoch in range(1000):
        model.train()
        
        for batch_index in range(10):
            data,label=dm.next_batch_train(batch_size=batch_size)
            data, target = torch.FloatTensor(data), torch.FloatTensor(label)
            #data, target = data.cuda(), target.cuda()
            data, target = Variable(data), Variable(target)
            optimizer.zero_grad()
            _,output = model(data)
            pred = output.gt(0).int().flatten()
            correct = pred.eq(target.int()).cpu().sum()
            loss = cost(output.flatten(), target)
            loss.backward()
            optimizer.step()
            print(str(epoch*5+batch_index),'train loss: {:.4f}, accuracy: {}/{} ({:.3f}%)'.format(loss.data.item(),correct,batch_size,100.*correct/batch_size))

        if model_path is not None:   
            torch.save(model.state_dict(),model_path)
            print("save:",model_path)

def infer(data,model_path):
    if 'MLP_2x5x1' in model_path:
        model = MLP_2x5x1()
    else:
        print("no model found.")
        return None,None,None
    assert os.path.exists(model_path),"%s not exist."%(model_path)
    
    sd=torch.load(model_path)
    model.load_state_dict(sd)
    model.eval()
    data = Variable(torch.FloatTensor(data))
    h,output = model(data,True)
    if isinstance(h,list):
        h = [x.data.cpu().numpy() for x in h]
    else:
        h = h.data.cpu().numpy()
    y = output.data.cpu().numpy()
    return h,y,(y>0.5).astype(np.int32)

        
    

if __name__=="__main__":
    
    train(MLP_2x5x1(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x5x1.pth")
    #train(MLP_2x3x1(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x3x1.pth")  
    #train(MLP_2x10x1(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x10x1.pth")
      
       

