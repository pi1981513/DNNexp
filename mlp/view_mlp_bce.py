import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import torch
from dataset.show_data import colors

def view_result_mlp2x5x1(data_path,model_path,img_save_path="",constraint_domain=False,show_hidden=True):
    '''
    constraint_domain: constraint fc layer input domain
    '''
    from train_mlp_bce import infer
    pm=torch.load(model_path)
    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    for k,v in pm.items():
        print(k,v)

    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    wr = w2[0]
    br = b2[0]
    print("wr hyperplane:",wr,br)

    #encode linear regions
    Eks = []
    for i in range(2):
        for j in range(2):
            for k in range(2):
                for l in range(2):
                    for m in range(2):
                        Eks.append([i,j,k,l,m])

    code_color_map = dict()
    #plot sub-region decision hyperplane
    for index,Ek in enumerate(Eks):
        Dk = np.diag(Ek)
        #print(Ek,wr,w0)
        w = np.dot(np.dot(wr.T,Dk),w1)
        b = np.dot(np.dot(wr.T,Dk),b1) + br
        print('hyperplane final:%s'%(','.join(map(str,Ek))),w,b,colors[index%len(colors)])

        x0=np.linspace(0.0,1.0,1000)
        x1=-(w[0]*x0+b)/w[1]

        input=list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                input.append([xx,yy])
        if len(input)==0:
            continue
        h,y,c = infer(input,model_path=model_path)
        codes = (h>0).astype(np.int32)

        #plot hyperplanes
        x,y=list(),list()
        for xy,code in zip(input,codes):
            if not constraint_domain:
                x.append(xy[0])
                y.append(xy[1])
            else:
                #constraint hyperplanes in sub-region
                if "".join(map(str,code))=="".join(map(str,Ek)):
                    x.append(xy[0])
                    y.append(xy[1])
        
        plt.plot(x,y,c=colors[index%len(colors)])
        code_color_map["".join(map(str,Ek))]=colors[index%len(colors)]

    #plot hidden hyperplanes
    x0=np.linspace(0.0,1.0,1000)
    index=0
    for w,b in zip(w1,b1):
        print("hidden node:%s"%(index),w,b,colors[index])
        x1=-(w[0]*x0+b)/w[1]

        x,y=list(),list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                x.append(xx)
                y.append(yy)
        if show_hidden:
            
            plt.plot(x,y,'--',c=colors[index])
        index+=1

    #plot data points
    data=dict()
    input=list()
    label=list()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
            input.append([float(arr[0]),float(arr[1])])
            label.append(l)

    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        
        plt.scatter(x,y,c=colors[k])

    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

if __name__=="__main__":
    
    view_result_mlp2x5x1(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_2x5x1.pth",
              constraint_domain=True,
              show_hidden=True
              )
