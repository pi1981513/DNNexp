import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import torch
from dataset.show_data import colors
import matplotlib.patches as patches
import cv2
import torch.nn.functional as F
from utils import alpha_polygon
from linear_regions import ch_concave_hull

def view_decision_boundary_mlp(data_path,model_path,img_save_path=""):
    
    from train_mlp_cdiff_afs import infer
    
    #plot forward real decision region
    num_data = 2000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path)
    
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    plt.scatter(xxs,xys,c='black',s=1)

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])
        
    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()
        
def activation_pattern_str(ap):
    return ''.join(list(map(str,ap)))

def ap_diag(ap_str,slop=0):
    ap = list(map(int,list(ap_str)))
    M = np.diag(np.array(ap)+slop*(np.array(ap)-1))
    return M

def ap_diag2(aps,slop=0):
    Ms = list()
    for ap in aps:
        M = np.diag(np.array(ap)+slop*(np.array(ap)-1))
        Ms.append(M)
    return np.array(Ms)

def plot_fc(xs,hs,ws,bs,ax):
    xs = np.asarray(xs,np.float32)

    hs = np.asarray(hs,np.float32)
    ws = np.asarray(ws,np.float32)
    bs = np.asarray(bs,np.float32)

    print(xs.shape,ws.shape,bs.shape)
    ws = ws[0] - ws[1]
    bs = bs[0] - bs[1]

    as_ = hs @ ws.T + bs
    activation_patterns = (as_>0).astype(np.int32)

    ap_map = dict()
    hs_map = dict()
    for ap,x,a in zip(activation_patterns,xs,as_):
        apstr = activation_pattern_str([ap])
        
        if ap_map.get(apstr) is None:
            ap_map[apstr] = list()
            hs_map[apstr] = list()
        
        ap_map[apstr].append(x)
    
    for index,ap in enumerate(ap_map.keys()):
        print("ap:",ap)
        
        x = ap_map.get(ap)
        x = np.asarray(x,dtype=np.float32)

        # may not convex when transformed with no-linear function
        #vert = cv2.convexHull(x.astype(np.float32))
        vert = ch_concave_hull(x.astype(np.float32),length_threshold=0.01)
        vert = np.reshape(vert,(vert.shape[0],2))
        
        polygon = patches.Polygon(vert,alpha=0.5,color=colors[int(ap)])
        ax.add_patch(polygon)


def plot_hidden(x_subspace,hidden_input,ws,bs,ax,slop=0,act=None):
    x_subspace = np.asarray(x_subspace,np.float32)
    hs = np.asarray(hidden_input,np.float32)

    ws = np.asarray(ws,np.float32)
    bs = np.asarray(bs,np.float32)

    #print(x_subspace.shape,ws.shape,bs.shape)
    as_ = hs @ ws.T + bs
    activation_patterns = (as_>0).astype(np.int32)

    #ap map: activation pattern->corresponding subspace input values
    #ys map: activation pattern->corresponding hidden output values
    ap_map = dict()
    ys_map = dict()
    for ap,x,a in zip(activation_patterns,x_subspace,as_):
        apstr = activation_pattern_str(ap)
        
        if ap_map.get(apstr) is None:
            ap_map[apstr] = list()
            ys_map[apstr] = list()
        
        ap_map[apstr].append(x)
        if act is not None:
            v = a @ ap_diag(apstr,slop=slop)
            y = act(torch.tensor(v)).numpy()
            ys_map[apstr].append( y)
        else:
            ys_map[apstr].append( a @ ap_diag(apstr,slop=slop))
    
    for index,ap in enumerate(ap_map.keys()):
        print("ap:",ap,"color:",colors[index%len(colors)])
        xs = ap_map.get(ap)
        xs = np.asarray(xs,dtype=np.float32)

        #vert = cv2.convexHull(xs.astype(np.float32))
        vert = ch_concave_hull(xs.astype(np.float32),length_threshold=0.01)
        vert = np.reshape(vert,(vert.shape[0],2))
        
        polygon = patches.Polygon(vert,alpha=0.5,color=colors[index%len(colors)])
        ax.add_patch(polygon)

    return ys_map,ap_map
    
        
def show_2n2():
    view_decision_boundary_mlp(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_2x3x2_silu.pth"
              )

    model_path = "../model/MLP_2x3x2_silu.pth"
    data_path="../dataset/2d_data_nc_2_circle.txt"
    pm=torch.load(model_path)

    for k,v in pm.items():
        print(k,v)

    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    
    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    range_x = np.linspace(0,1,1000)
    range_y = np.linspace(0,1,1000)
    xs = list()
    for x in range_x:
        for y in range_y:
            xs.append([x,y])
    
    fig = plt.figure()
    ax = fig.add_subplot(2,2,1)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)

    ys_map,ap_map = plot_hidden(xs,xs,w1,b1,ax=ax,slop=-1,act=F.silu)

    ax = fig.add_subplot(2,2,2)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    for ap,xs in ap_map.items():
        print("ap:",ap)
        ys = ys_map.get(ap)
        plot_fc(xs,ys,w2,b2,ax)

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[-k])
        
    plt.show()

def show_2nn2():
    view_decision_boundary_mlp(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_2x3x3x2_silu.pth"
              )

    model_path = "../model/MLP_2x3x3x2_silu.pth"
    data_path="../dataset/2d_data_nc_2_circle.txt"
    pm=torch.load(model_path)
    for k,v in pm.items():
        print(k,v)

    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    
    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    w3=pm["fc3.weight"].cpu().numpy()
    b3=pm["fc3.bias"].cpu().numpy()

    range_x = np.linspace(0,1,1000)
    range_y = np.linspace(0,1,1000)
    xs = list()
    for x in range_x:
        for y in range_y:
            xs.append([x,y])
    
    fig = plt.figure()
    ax = fig.add_subplot(2,2,1)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)

    hs_map,ap_map = plot_hidden(xs,xs,w1,b1,ax=ax,slop=-1,act=F.silu)

    ax1 = fig.add_subplot(2,2,2)
    ax1.set_xlim(0,1)
    ax1.set_ylim(0,1)

    ax2 = fig.add_subplot(2,2,3)
    ax2.set_xlim(0,1)
    ax2.set_ylim(0,1)

    for ap,xs in ap_map.items():
        print("ap:",ap)
        hs = hs_map.get(ap)
        #plot_fc(xs,hs,w2,b2,ax1)
        hs_map1,ap_map1 = plot_hidden(xs,hs,w2,b2,ax=ax1,slop=-1,act=F.silu)
        for ap1,xs1 in ap_map1.items():
            hs1 = hs_map1.get(ap1)
            plot_fc(xs1,hs1,w3,b3,ax2)

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[-k])
        
    plt.show()

if __name__=="__main__":
    #show_2n2()
    show_2nn2()

    