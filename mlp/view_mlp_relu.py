import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import torch
from dataset.show_data import colors

def plot_hidden(w,b,hidden_out,clazz):
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
            
    x,y = np.meshgrid(range(11),range(11))
    w=np.array(w)[0]-np.array(w)[1]
    b=b[0]-b[1]
    z=(w[0]*x+w[1]*y+b)/(-w[2])
    ax.plot_surface(x, y, z, alpha=0.5,color=colors[5])
    
    xyz0 = list()
    xyz1 = list()
    for xyz,cc in zip(hidden_out,clazz):
        if cc==0:
            xyz0.append(xyz)
        else:
            xyz1.append(xyz)
    xyz0 = np.array(xyz0)
    xyz1 = np.array(xyz1)
    print(xyz0[:,0].shape,xyz0[:,1].shape,xyz0[:,2].shape)
    x = [a for a in xyz0[:,0]]
    y = [a for a in xyz0[:,1]]
    z = [a for a in xyz0[:,2]]
    ax.scatter(x,y,z, c=colors[0])
    x = [a for a in xyz1[:,0]]
    y = [a for a in xyz1[:,1]]
    z = [a for a in xyz1[:,2]]
    ax.scatter(x,y,z, c=colors[1])
    
    ax.legend()
    #plt.savefig('3d_fig.png',dpi=200)
    plt.show()

def view_decision_boundary_mlp(data_path,model_path,img_save_path=""):
    
    from train_mlp_relu import infer
    
    #plot forward real decision region
    num_data = 2000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path)
    
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    plt.scatter(xxs,xys,c='black',s=1)

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])
        
    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

def view_result_mlp2x3x2(data_path,model_path,img_save_path="",constraint_domain=False,show_hidden=True):
    '''
    constraint_domain: constraint fc layer input domain
    '''
    from train_mlp_relu import infer
    pm=torch.load(model_path)
    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    for k,v in pm.items():
        print(k,v)

    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    wr = w2[0] - w2[1]
    br = b2[0] - b2[1]
    print("wr hyperplane:",wr,br)

    #encode linear regions
    Eks = []
    for i in range(2):
        for j in range(2):
            for k in range(2):
                Eks.append([i,j,k])

    #plot sub-region decision hyperplane
    for index,Ek in enumerate(Eks):
        Dk = np.diag(Ek)
        #print(Ek,wr,w0)
        w = np.dot(np.dot(wr.T,Dk),w1)
        b = np.dot(np.dot(wr.T,Dk),b1) + br
        print('hyperplane final:%s'%(','.join(map(str,Ek))),w,b,colors[index])
        
        x0=np.linspace(0.0,1.0,1000)
        x1=-(w[0]*x0+b)/w[1]

        input=list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                input.append([xx,yy])
        if len(input)==0:
            continue
        h,y,c = infer(input,model_path=model_path)
        codes = (h>0).astype(np.int32)
        
        #plot hyperplanes
        x,y=list(),list()
        for xy,code in zip(input,codes):
            if not constraint_domain:
                x.append(xy[0])
                y.append(xy[1])
            else:
                if "".join(map(str,code))=="".join(map(str,Ek)):
                    x.append(xy[0])
                    y.append(xy[1])
        plt.plot(x,y,c=colors[index])
    
    #plot forward decision boundary
    num_data = 1000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path)
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    #plt.scatter(xxs,xys,c='black',s=1)

    #plot hidden hyperplanes
    x0=np.linspace(0.0,1.0,1000)
    index=0
    for w,b in zip(w1,b1):
        print("hidden node:%s"%(index),w,b,colors[index])
        x1=-(w[0]*x0+b)/w[1]
        
        x,y=list(),list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                x.append(xx)
                y.append(yy)
        if show_hidden:
            plt.plot(x,y,'--',c=colors[index])
        index+=1

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])
        
    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

def view_result_mlp2x10x2(data_path,model_path,img_save_path="",constraint_domain=False):
    '''
    constraint_domain: constraint fc layer input domain
    '''
    from train_mlp_relu import infer
    pm=torch.load(model_path)
    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    for k,v in pm.items():
        print(k,v)

    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    wr = w2[0] - w2[1]
    br = b2[0] - b2[1]
    print("wr hyperplane:",wr,br)

    #encode linear regions
    Eks = []
    for i in range(2):
        for j in range(2):
            for k in range(2):
                for l in range(2):
                    for m in range(2):
                        for n in range(2):
                            for o in range(2):
                                for p in range(2):
                                    for q in range(2):
                                        for r in range(2):
                                            Eks.append([i,j,k,l,m,n,o,p,q,r])

    #plot sub-region decision hyperplane
    for index,Ek in enumerate(Eks):
        Dk = np.diag(Ek)
        #print(Ek,wr,w0)
        w = np.dot(np.dot(wr.T,Dk),w1)
        b = np.dot(np.dot(wr.T,Dk),b1) + br
        print('hyperplane final:%s'%(','.join(map(str,Ek))),w,b,colors[-1])
        
        x0=np.linspace(0.0,1.0,1000)
        x1=-(w[0]*x0+b)/w[1]

        input=list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0:
                input.append([xx,yy])
        if len(input)==0:
            continue
        h,y,c = infer(input,model_path=model_path)
        codes = (h>0).astype(np.int32)
        
        #plot hyperplanes
        x,y=list(),list()
        for xy,code in zip(input,codes):
            if not constraint_domain:
                x.append(xy[0])
                y.append(xy[1])
            else:
                if "".join(map(str,code))=="".join(map(str,Ek)):
                    x.append(xy[0])
                    y.append(xy[1])
        plt.plot(x,y,c=colors[-1])
    
    #plot forward decision boundary
    num_data = 1000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path)
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    plt.scatter(xxs,xys,c='black',s=1)

    #plot hidden hyperplanes
    x0=np.linspace(0.0,1.0,1000)

    corner = [[0,0],[0,1],[1,0],[1,1]]
    index=0
    for w,b in zip(w1,b1):
        print("hidden node:%s"%(index),w,b,colors[index])
        x1=-(w[0]*x0+b)/w[1]

        cor_out = np.dot(w,np.asarray(corner,dtype=np.float32).T)+b
        flag = np.sum((cor_out>0).astype(np.int32))

        print(cor_out,flag)
        
        x,y=list(),list()
        for xx,yy in zip(x0,x1):
            if yy<1.0 and yy>0:
                x.append(xx)
                y.append(yy)
        
        
        #if flag!=0 and flag!=4:
        plt.plot(x,y,'--',c=colors[index])
        index+=1

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])
        
    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

def view_result_mlp2x3x3x2(data_path,model_path,img_save_path="",constraint_domain=True,show_hidden=True):
    '''
    constraint_domain: constraint fc layer input domain
    '''
    from train_mlp_relu import infer
    pm=torch.load(model_path)
    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    for k,v in pm.items():
        print(k,v)

    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    #classification weights
    w3=pm["fc3.weight"].cpu().numpy()
    b3=pm["fc3.bias"].cpu().numpy()

    wr = w3[0] - w3[1]
    br = b3[0] - b3[1]
    print("wr hyperplane:",wr,br)

    #encode linear regions
    E1ks = []
    E2ks = []
    for i in range(2):
        for j in range(2):
            for k in range(2):
                E1ks.append([i,j,k])
                E2ks.append([i,j,k])

    #plot sub-region decision hyperplane
    index = 0
    for i,E1k in enumerate(E1ks):

        D1k = np.diag(E1k)
        w_h = np.dot(np.dot(w2,D1k),w1)
        b_h = np.dot(np.dot(w2,D1k),b1) + b2
        print('hidden node2:%s'%(','.join(map(str,E1k))),w_h,b_h,colors[-2])

        x0=np.linspace(0.0,1.0,1000)
        for ww,bb in zip(w_h,b_h):
            x1=-(ww[0]*x0+bb)/ww[1]
            input=list()
            for xx,yy in zip(x0,x1):
                if yy<=1.0 and yy>=0:
                    input.append([xx,yy])
            if len(input)==0:
                continue
            h,y,c = infer(input,model_path=model_path)
            codes1 = (h[0]>0).astype(np.int32)
            x,y=list(),list()
            for xy,code1 in zip(input,codes1):
                if not constraint_domain:
                    x.append(xy[0])
                    y.append(xy[1])
                else:
                    if ("".join(map(str,code1))=="".join(map(str,E1k))):
                        x.append(xy[0])
                        y.append(xy[1])
            if show_hidden:
                plt.plot(x,y,'--',c=colors[-2])

        for j,E2k in enumerate(E2ks):
            
            D2k = np.diag(E2k)
            #print('wr',wr.shape,'w1',w1.shape,'w2',w2.shape)
            w = np.dot(np.dot(np.dot(np.dot(wr.T,D2k),w2),D1k),w1)
            b = np.dot(np.dot(np.dot(np.dot(wr.T,D2k),w2),D1k),b1) + np.dot(np.dot(wr.T,D2k),b2) + br
            print('hyperplane final:%s_%s'%(','.join(map(str,E1k)),','.join(map(str,E2k))),w,b,colors[index%len(colors)])
            
            x1=-(w[0]*x0+b)/w[1]

            input=list()
            for xx,yy in zip(x0,x1):
                if yy<=1.0 and yy>=0:
                    input.append([xx,yy])
            if len(input)==0:
                continue
            h,y,c = infer(input,model_path=model_path)
            codes1 = (h[0]>0).astype(np.int32)
            codes2 = (h[1]>0).astype(np.int32)

            #plot decision hyperplanes
            x,y=list(),list()
            for xy,code1,code2 in zip(input,codes1,codes2):
                if not constraint_domain:
                    x.append(xy[0])
                    y.append(xy[1])
                else:
                    if ("".join(map(str,code1))=="".join(map(str,E1k))
                        and "".join(map(str,code2))=="".join(map(str,E2k))):
                        x.append(xy[0])
                        y.append(xy[1])
            plt.plot(x,y,c=colors[index%len(colors)])
            index+=1

    #plot forward decision boundary
    num_data = 1000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path)
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    #plt.scatter(xxs,xys,c='black',s=1)

    #plot first hidden hyperplanes
    x0=np.linspace(0.0,1.0,10000)
    index=0
    for w,b in zip(w1,b1):
        print("hidden node1:%s"%(index),w,b,colors[-3])
        x1=-(w[0]*x0+b)/w[1]
        
        x,y=list(),list()
        for xx,yy in zip(x0,x1):
            if yy<=1.0 and yy>=0.0:
                x.append(xx)
                y.append(yy)
        if show_hidden:
            plt.plot(x,y,'--',c=colors[-3])
        index+=1

    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])
        
    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()
        
if __name__=="__main__":
    
    view_result_mlp2x3x2(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_2x3x2.pth",
              constraint_domain=True,
              show_hidden=True
              )
    
    # view_result_mlp2x10x2(
    #           data_path="../dataset/2d_data_nc_2_circle.txt",
    #           model_path="../model/MLP_2x10x2.pth",
    #           constraint_domain=False
    #           )
    # view_decision_boundary_mlp(
    #           data_path="../dataset/2d_data_nc_2_circle.txt",
    #           model_path="../model/MLP_2x3x3x2.pth"
    #           )
    view_result_mlp2x3x3x2(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_2x3x3x2.pth",
              constraint_domain=True,
              show_hidden=True
              )
