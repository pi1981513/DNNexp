import argparse
import torch
import torch.nn
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.cuda
import torch.utils.data
import os
import file as fu
import numpy as np

from data import DataManager
import view_fc 

class MLP_2xN(nn.Module):
    
    def __init__(self,n_out = 2):
        super(MLP_2xN, self).__init__()
        self.fc1=nn.Linear(2,n_out)

    def forward(self, x):
        x = self.fc1(x)
        return x

def train(model:nn.Module,data_path,model_path=None,save_hyperplane_image=False):
    
    dm=DataManager(data_path)
   
    #model.cuda()
    cost = torch.nn.CrossEntropyLoss()#.cuda()
    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
    batch_size=200
    
    for k,v in model.named_parameters():
        print(k,v)
    
    for epoch in range(1000):
        model.train()
        
        for batch_index in range(10):
            data,label=dm.next_batch_train(batch_size=batch_size)
            data, target = torch.FloatTensor(data), torch.LongTensor(label)
            #data, target = data.cuda(), target.cuda()
            data, target = Variable(data), Variable(target)
            optimizer.zero_grad()
            output = model(data)
            pred = output.data.max(1)[1]
            correct = pred.eq(target.data).cpu().sum()
            loss = cost(output, target)
            loss.backward()
            optimizer.step()
            print(str(epoch*5+batch_index),'train loss: {:.4f}, accuracy: {}/{} ({:.3f}%)'.format(loss.data.item(),correct,batch_size,100.*correct/batch_size))

        if model_path is not None:   
            torch.save(model.state_dict(),model_path)
            print("save:",model_path)

        if save_hyperplane_image and epoch%2==0:
            view_fc.view_result_mlp2xN(data_path,model_path,img_save_path="../images/000%s.jpg"%(epoch))

         

if __name__=="__main__":
    train(MLP_2xN(n_out=2),'../dataset/2d_data_nc_2.txt',"../model/MLP_2x2.pth") 
    train(MLP_2xN(n_out=3),'../dataset/2d_data_nc_3.txt',"../model/MLP_2x3.pth") 
    train(MLP_2xN(n_out=4),'../dataset/2d_data_nc_4.txt',"../model/MLP_2x4.pth")  
    train(MLP_2xN(n_out=5),'../dataset/2d_data_nc_5.txt',"../model/MLP_2x5.pth")    

