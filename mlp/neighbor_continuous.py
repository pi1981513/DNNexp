import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import torch
from dataset.show_data import colors

def find_neighbor_pair(hyperplane_wb,U_range=[0,1],point_dist = 0.001):
    assert len(U_range)==2,"bound len should 2"
    assert point_dist>0,"point_dist should > 0"
    lower,upper = U_range

    wb = hyperplane_wb
    space_pts = list()
    sample_xy = (upper - lower)/point_dist
    for x0 in np.linspace(lower,upper,sample_xy):
        for x1 in np.linspace(lower,upper,sample_xy):

            z = wb[0]*x0 + wb[1]*x1 + wb[2]
            if z > 0 and z< point_dist:
                space_pts.append([x0,x1])

    return space_pts