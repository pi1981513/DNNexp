from scipy.special import comb
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as patches
from scipy.spatial import ConvexHull
import cv2

from concave_hull import concave_hull, concave_hull_indexes
from shapely import box, LineString, normalize, Polygon,intersection

def max_linear_regions(dim,m):
    '''
    dim: input dimension
    m: number hyperplanes
    '''
    max_sep = 0
    for i in range(dim+1):
        max_sep += comb(m,i)
    return max_sep

def fc_hyperplanes(nodes):
    '''
    fully connected layer's hyperplanes
    nodes: number nodes
    '''
    return comb(nodes,2)

def ch_concave_hull(points,length_threshold=0.1):
    vert = concave_hull(points, length_threshold=length_threshold)
    vert = np.reshape(vert,(vert.shape[0],2))
    return vert

def halfspace(hyperplane_wb,positive=True,U_range=[0,1],sample_xy=500):
    assert len(U_range)==2,"bound len should 2"
    lower,upper = U_range

    wb = hyperplane_wb
    space_pts = list()
    for x0 in np.linspace(lower,upper,sample_xy):
        for x1 in np.linspace(lower,upper,sample_xy):

            z = wb[0]*x0 + wb[1]*x1 + wb[2]
            if positive and z > 0:
                space_pts.append([x0,x1])
            elif not positive and z <= 0:
                space_pts.append([x0,x1])
                
    pts = np.array(space_pts)
    vert = cv2.convexHull(pts.astype(np.float32))
    vert = np.reshape(vert,(vert.shape[0],2))    
    return vert

def region_intersec(region_A,region_B):

    poly1 = Polygon(region_A)
    poly2 = Polygon(region_B)
    
    isIntersection = intersection(poly1,poly2)
    intersec = np.array(isIntersection.exterior.coords)
    
    vert = np.array(intersec,dtype=np.float32)
    vert = np.reshape(vert,(vert.shape[0],2))    
    return vert

def regions_intersec(regions):
    intersec = regions[0]
    for p in regions[1:]:
        intersec = region_intersec(intersec,p)
    return intersec