import numpy as np
import matplotlib.pylab as plt
from sklearn.cluster import KMeans 
import file as fu

def gen_2d_data(categories,path):
    
    km=KMeans(n_clusters=categories)
    x=np.random.rand(300,2)
    
    km.fit(x)
    labels=km.predict(x)
    
    print(labels)
    xy=[(v,l) for v,l in zip(x,labels)]
    xy.sort(key=lambda x:x[1])
    
    data=dict()
    for v,l in xy:
        if data.get(l) is None:
            data[l]=list()
        data[l].append(v)
        
    cs=['b','g','r','m','y','k']
    
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=cs[k])
    
    
    fu.write(path)
    for k,v in data.items():
        for x,y in v:
            fu.append(path, "%s,%s,%s\n"%(x,y,k))
    
    plt.show()
    
def gen_2d_circle_data(path="../dataset/2d_data_circle.txt"):
    
    x=np.random.rand(300,2)
    m=np.mean(x,axis=0)
    radius=0.4
    cs=['b','g','r','m','y','k']
    
    for d in x:
        dis=np.sum(np.abs(d-m))
        if dis<radius:
            plt.scatter([d[0]],[d[1]],c=cs[2])
            fu.append(path, "%s,%s,%s\n"%(d[0],d[1],0))
        else:
            fu.append(path, "%s,%s,%s\n"%(d[0],d[1],1))
            plt.scatter([d[0]],[d[1]],c=cs[1])
    
    plt.show()
    
class DataManager():
    
    def __init__(self,path,shuffle=True):
        x1=list()
        x2=list()
        y=list()
        with open(path) as f:
            for line in f:
                arr=line.strip().split(",")
                x1.append(float(arr[0]))
                x2.append(float(arr[1]))
                y.append(int(arr[2]))
        data=[[a,b] for a,b in zip(x1,x2)]
        label=y
        self.data=list(zip(data,label))
        if shuffle:
            np.random.shuffle(self.data)
        self.batch_index=0
    
    def next_batch_train(self,batch_size=30):
        if (self.batch_index+1)*batch_size>=len(self.data):
            self.batch_index=0
            np.random.shuffle(self.data)
        d,l=list(),list()
        for x,y in self.data[self.batch_index*batch_size:(self.batch_index+1)*batch_size]:
            d.append(x)
            l.append(y)
        self.batch_index+=1
        return d,l
    
    def data_all(self):
        d=list()
        l=list()
        for x,y in self.data:
            d.append(x)
            l.append(y)
        self.batch_index+=1
        return d,l
            
    
if __name__=="__main__":
    category=3
    gen_2d_data(categories=category,
                path="../dataset/2d_data_nc_%s.txt"%(category)
                )
    #gen_2d_circle_data()
    
    