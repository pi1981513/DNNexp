import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import file as fu
import torch
from dataset.show_data import colors

def view_result_mlp2x4x2(data_path,model_path,img_save_path="",show_node_line=True):
    
    from mlp.train_mlp_soft_maxout import infer
    pm=torch.load(model_path)
    ws=pm["fc1.weight"].cpu().numpy()
    bs=pm["fc1.bias"].cpu().numpy()
    print(ws,bs)

    wr = ws[0] - ws[1]
    br = bs[0] - bs[1]
    print("wr hyperplane:",wr,br)

    wr2 = ws[2] - ws[3]
    br2 = bs[2] - bs[3]
    print("wr hyperplane:",wr2,br2)
    #for k,v in pm.items():
    #    print(k,v)

    ws2=pm["fc2.weight"].cpu().numpy()
    bs2=pm["fc2.bias"].cpu().numpy()

    wr_c = ws2[0] - ws2[1]
    br_c = bs2[0] - bs2[1]

    print("wr_c hyperplane:",wr_c,br_c)
    
    #plot decision hyperplanes
    x0=np.linspace(0.0,1.0,1000)
    index = 0
    for w,b in zip([wr,wr2],[br,br2]):
        color = colors[(index)%len(colors)]
        print('hyperplane[%s]:'%(index),w,b,color)
        x1=(w[0]*x0+b)/(-w[1])

        xs = list()
        ys = list()
        for xx,yy in zip(x0,x1):
            if yy<=1.1 and yy>=-0.1:
                xs.append(xx)
                ys.append(yy)
        plt.subplot(2,2,1)
        plt.plot(xs,ys,'--',c=color)
        index+=1

    #load data points
    data=dict()
    input=list()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
            input.append([float(arr[0]),float(arr[1])])
    
    #plot data points
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.subplot(2,2,1)
        plt.scatter(x,y,c=colors[k])

    h,y,c = infer(input,model_path=model_path)

    #plot hidden data
    for ab,cc in zip(h,c):
        plt.subplot(2,2,2)
        a,b = ab
        print(cc,ab)
        plt.scatter(a,b,c=colors[int(cc)])

    #plot hidden out decision boundry
    x0=np.linspace(-1.5,2.5,1000)
    index = 0
    for w,b in zip([wr_c],[br_c]):
        color = colors[(index+len(ws))%len(colors)]
        print('hyperplane[%s]:'%(index),w,b,color)
        x1=(w[0]*x0+b)/(-w[1])

        xs = list()
        ys = list()
        for xx,yy in zip(x0,x1):
            if yy<=2.5 and yy>=-1.5:
                xs.append(xx)
                ys.append(yy)
        plt.subplot(2,2,2)
        plt.plot(xs,ys,c=color)
        index+=1

    #plot forward decision boundary
    num_data = 2000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path)
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    plt.subplot(2,2,1)
    plt.scatter(xxs,xys,c='red',s=1)

    #plot forward decision boundary
    num_data = 2000
    v=np.linspace(0.0,1.0,num_data)
    input = list()
    for a in v:
        for b in v:
            input.append([a,b])
    h,y,c = infer(input,model_path=model_path.replace("S_","M_"))
    yy = y[:,0]-y[:,1]
    xxs = list()
    xys = list()
    for xx,y0 in zip(input,yy):
        if y0**2<0.001:
            xxs.append(xx[0])
            xys.append(xx[1])
    plt.subplot(2,2,1)
    plt.scatter(xxs,xys,c='black',s=1)

    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

    
if __name__=="__main__":
    view_result_mlp2x4x2(
              data_path="../dataset/2d_data_nc_2_circle.txt",
              model_path="../model/MLP_S_2x4x2.pth",
              show_node_line=True
              )
    
    
