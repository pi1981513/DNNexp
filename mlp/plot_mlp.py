import sys
sys.path.insert(0,"../")
import matplotlib.pylab as plt
import matplotlib.patches as patches
import numpy as np
import torch
import cv2

colors=['blue','green','pink','cyan','magenta','yellow','black','red','gray','gold']
        
def activation_pattern_str(ap):
    return ''.join(list(map(str,ap)))

def ap_diag(ap_str,slop=0):
    ap = list(map(int,list(ap_str)))
    M = np.diag(np.array(ap)+slop*(np.array(ap)-1))
    return M

def ap_diag2(aps,slop=0):
    Ms = list()
    for ap in aps:
        M = np.diag(np.array(ap)+slop*(np.array(ap)-1))
        Ms.append(M)
    return np.array(Ms)

def plot_fc(xs,hs,ws,bs,ax):
    xs = np.asarray(xs,np.float32)

    hs = np.asarray(hs,np.float32)
    ws = np.asarray(ws,np.float32)
    bs = np.asarray(bs,np.float32)

    print(xs.shape,ws.shape,bs.shape)
    ws = ws[0] - ws[1]
    bs = bs[0] - bs[1]

    as_ = hs @ ws.T + bs
    activation_patterns = (as_>0).astype(np.int32)

    ap_map = dict()
    for ap,x,a in zip(activation_patterns,xs,as_):
        apstr = activation_pattern_str([ap])
        
        if ap_map.get(apstr) is None:
            ap_map[apstr] = list()
            ys_map[apstr] = list()
        
        ap_map[apstr].append(x)
    
    for index,ap in enumerate(ap_map.keys()):
        print("ap:",ap)
        xs = ap_map.get(ap)
        xs = np.asarray(xs,dtype=np.float32)

        vert = cv2.convexHull(xs.astype(np.float32))
        vert = np.reshape(vert,(vert.shape[0],2))
        
        polygon = patches.Polygon(vert,alpha=0.5,color=colors[int(ap)])
        ax.add_patch(polygon)


def plot_hidden(x_subspace,hidden_input,ws,bs,ax,slop=0):
    x_subspace = np.asarray(x_subspace,np.float32)
    hs = np.asarray(hidden_input,np.float32)

    ws = np.asarray(ws,np.float32)
    bs = np.asarray(bs,np.float32)

    #print(x_subspace.shape,ws.shape,bs.shape)
    as_ = hs @ ws.T + bs
    activation_patterns = (as_>0).astype(np.int32)

    #ap map: activation pattern->corresponding subspace input values
    #ys map: activation pattern->corresponding hidden output values
    ap_map = dict()
    ys_map = dict()
    for ap,x,a in zip(activation_patterns,x_subspace,as_):
        apstr = activation_pattern_str(ap)
        
        if ap_map.get(apstr) is None:
            ap_map[apstr] = list()
            ys_map[apstr] = list()
        
        ap_map[apstr].append(x)
        ys_map[apstr].append( a @ ap_diag(apstr,slop=slop))
    
    for index,ap in enumerate(ap_map.keys()):
        print("ap:",ap,"color:",colors[index%len(colors)])
        xs = ap_map.get(ap)
        xs = np.asarray(xs,dtype=np.float32)

        vert = cv2.convexHull(xs.astype(np.float32))
        vert = np.reshape(vert,(vert.shape[0],2))
        
        polygon = patches.Polygon(vert,alpha=0.5,color=colors[index%len(colors)])
        ax.add_patch(polygon)

    return ys_map,ap_map

if __name__=="__main__":
    model_path = "../model/MLP_2x3x2_dual_relu.pth"
    data_path="../dataset/2d_data_nc_2_circle.txt"
    pm=torch.load(model_path)
    #hidden weights
    w1=pm["fc1.weight"].cpu().numpy()
    b1=pm["fc1.bias"].cpu().numpy()
    for k,v in pm.items():
        print(k,v)

    #classification weights
    w2=pm["fc2.weight"].cpu().numpy()
    b2=pm["fc2.bias"].cpu().numpy()

    wr = w2[0] - w2[1]
    br = b2[0] - b2[1]
    print("wr hyperplane:",wr,br)

    range_x = np.linspace(0,1,1000)
    range_y = np.linspace(0,1,1000)
    xs = list()
    for x in range_x:
        for y in range_y:
            xs.append([x,y])
    
    fig = plt.figure()
    ax = fig.add_subplot(2,2,1)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)

    ys_map,ap_map = plot_hidden(xs,xs,w1,b1,ax=ax,slop=-0.1)

    ax = fig.add_subplot(2,2,2)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    for ap,xs in ap_map.items():
        ys = ys_map.get(ap)
        plot_fc(xs,ys,w2,b2,ax)


    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[-k])
        
    plt.show()