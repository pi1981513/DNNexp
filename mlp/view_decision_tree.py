import sys
sys.path.insert(0,"../")
from sklearn import tree
import numpy as np
import matplotlib.pylab as plt
from dataset.show_data import colors

def plot_boundary(clf):

    n_nodes = clf.tree_.node_count
    children_left = clf.tree_.children_left
    children_right = clf.tree_.children_right
    feature = clf.tree_.feature
    threshold = clf.tree_.threshold

    node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    stack = [(0, 0)]  # start with the root node id (0) and its depth (0)
    while len(stack) > 0:
        # `pop` ensures each node is only visited once
        node_id, depth = stack.pop()
        node_depth[node_id] = depth

        # If the left and right child of a node is not the same we have a split
        # node
        is_split_node = children_left[node_id] != children_right[node_id]
        # If a split node, append left and right children and depth to `stack`
        # so we can loop through them
        if is_split_node:
            stack.append((children_left[node_id], depth + 1))
            stack.append((children_right[node_id], depth + 1))
        else:
            is_leaves[node_id] = True

    print(
        "The binary tree structure has {n} nodes and has "
        "the following tree structure:\n".format(n=n_nodes)
    )

    for i in range(n_nodes):
        if is_leaves[i]:
            print(
                "{space} node={node} is a leaf node.".format(
                    space=node_depth[i], node=i
                )
            )
        else:
            print(
                "{space} node={node} is a split node: "
                "go to node {left} if X[:, {feature}] <= {threshold} "
                "else to node {right}.".format(
                    space=node_depth[i],
                    node=i,
                    left=children_left[i],
                    feature=feature[i],
                    threshold=threshold[i],
                    right=children_right[i],
                )
            )
    
    def recurse_tree(node_id,x_min,x_max,y_min,y_max):
        
        if is_leaves[node_id]:
            pass
        else:
            f_index = feature[node_id]
            thresh = threshold[node_id]
            left = children_left[node_id]
            right = children_right[node_id]
            
            if f_index==0:
                x = thresh
                xs = [x,x]
                ys = [y_min,y_max]
                #print("plot x=%s"%x)
                plt.plot(xs,ys,c=colors[node_id%len(colors)])
                recurse_tree(left,x_min,thresh,y_min,y_max)
                recurse_tree(right,thresh,x_max,y_min,y_max)
            else:
                y = thresh
                xs = [x_min,x_max]
                ys = [y,y]
                #print("plot y=%s"%y)
                plt.plot(xs,ys,c=colors[node_id%len(colors)])
                recurse_tree(left,x_min,x_max,y_min,thresh)
                recurse_tree(right,x_min,x_max,thresh,y_max)

    recurse_tree(0,0,1,0,1)
    plt.show()

def read_data(data_path):
    data=dict()
    X = list()
    y = list()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
            X.append([float(arr[0]),float(arr[1])])
            y.append(l)
    return X,y,data

if __name__=="__main__":
    X,y,dm = read_data("../dataset/2d_data_nc_2_circle.txt")
    clf = tree.DecisionTreeClassifier(criterion='entropy',max_depth=6,min_samples_split=10)
    clf = clf.fit(X,y)
    print("mean acc:",clf.score(X,y))
    #print(clf.predict([[0.0,0.2]]))
    print("tree structure:")
    print(tree.export_text(clf))

    #import graphviz
    #data = tree.export_graphviz(clf,out_file=None)
    #graph = graphviz.Source(data)
    #graph.render('test','../images')

    for k,v in dm.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])

    plot_boundary(clf)        
