import sys
sys.path.insert(0,"../")
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import file as fu
import torch
from dataset.show_data import colors
from linear_regions import halfspace,regions_intersec
import matplotlib.patches as patches

def view_result_mlp2xN(data_path,model_path,img_save_path="",show_node_line=True):
    
    pm=torch.load(model_path)
    ws=pm["fc1.weight"].cpu().numpy()
    bs=pm["fc1.bias"].cpu().numpy()
    #for k,v in pm.items():
    #    print(k,v)
    
    x0=np.linspace(0.0,1.0,10000)
    #plot node hyperplanes
    if show_node_line:
        index=0
        for w,b in zip(ws,bs):
            print("node:%s"%(index),w,b,colors[index])
            x1=-(w[0]*x0+b)/w[1]
            
            x,y=list(),list()
            for xx,yy in zip(x0,x1):
                if yy<=1.2 and yy>=-0.2:
                    x.append(xx)
                    y.append(yy)
            plt.plot(x,y,'--',c=colors[index])
            index+=1
    
    #plot decision hyperplanes
    index = 0
    for i in range(len(ws)):
        for j in range(i+1,len(ws),1):
            b = bs[i]-bs[j]
            w = ws[i]-ws[j]
            color = colors[(index+len(ws))%len(colors)]
            print('hyperplane[%s,%s]:'%(i,j),w,b,color)
            x1=(w[0]*x0+b)/(-w[1])

            xs = list()
            ys = list()
            for xx,yy in zip(x0,x1):
                if yy<=1.2 and yy>=-0.2:
                    xs.append(xx)
                    ys.append(yy)
            plt.plot(xs,ys,c=color)
            index+=1

    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])

    if img_save_path!="":
        plt.savefig(img_save_path,dpi=100)
        plt.close()
    else:
        plt.show()

def show_linear_regions(data_path,model_path,data_range=[0,1]):

    pm=torch.load(model_path)
    ws=pm["fc1.weight"].cpu().numpy()
    bs=pm["fc1.bias"].cpu().numpy()

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    for i in range(len(ws)):
        w0 = ws[i]
        b0 = bs[i]
        polys = list()
        for j in range(len(ws)):
            if i==j:
                continue
            w1 = ws[j]
            b1 = bs[j]
            w = w0 - w1
            b = b0 - b1
            poly0 = halfspace([w[0],w[1],b],positive=True,U=data_range)
            polys.append(poly0)

        intersec = regions_intersec(polys)
        
        polygon = patches.Polygon(intersec,alpha=0.1,color=colors[-i])
        ax.add_patch(polygon)
        
    #plot data points
    data=dict()
    with open(data_path) as f:
        for line in f:
            arr=line.strip().split(",")
            l = int(arr[2])
            if data.get(l) is None:
                data[l]=list()
            data[l].append([float(arr[0]),float(arr[1])])
         
    for k,v in data.items():
        x=[i for i,j in v]
        y=[j for i,j in v]
        plt.scatter(x,y,c=colors[k])
    plt.show()
    
if __name__=="__main__":
    view_result_mlp2xN(
              data_path="../dataset/2d_data_nc_2.txt",
              model_path="../model/MLP_2x2.pth",
              show_node_line=True
              )
    # view_result_mlp2xN(
    #           data_path="../dataset/2d_data_nc_3.txt",
    #           model_path="../model/MLP_2x3.pth",
    #           show_node_line=True
    #           )
    show_linear_regions(
              data_path="../dataset/2d_data_nc_2.txt",
              model_path="../model/MLP_2x2.pth",
              )
    show_linear_regions(
              data_path="../dataset/2d_data_nc_3.txt",
              model_path="../model/MLP_2x3.pth",
              )

    show_linear_regions(
              data_path="../dataset/2d_data_nc_4.txt",
              model_path="../model/MLP_2x4.pth",
              )
    show_linear_regions(
              data_path="../dataset/2d_data_nc_5.txt",
              model_path="../model/MLP_2x5.pth",
              )
    
