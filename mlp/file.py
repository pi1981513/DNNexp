import os 
import shutil

def write(path,data=''):
    index=path.rfind('/')
    dirs=path[:index+1]
    if not os.path.exists(dirs):
        os.makedirs(dirs)
        
    f = open(path,'w')
    f.write(data)
    f.close()

def append(path,data):
    f = open(path,'a')
    f.write(data)
    f.close()
    
def exist(path):
    return os.path.exists(path)


def move(src,dst):
    shutil.move(src, dst)