import argparse
import torch
import torch.nn
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.cuda
import torch.utils.data
import os
import file as fu
import numpy as np

from data import DataManager

class DualReLU(nn.Module):
    def __init__(self,slop = 0):
        super(DualReLU, self).__init__()

        self.relu_pos=nn.ReLU()
        self.relu_neg=nn.ReLU()
        self.alpha = slop

    def forward(self, x):
        
        h0 = self.relu_pos(x)
        h1 = self.alpha*self.relu_neg(-x)

        return h0+h1,h0,h1

class MLP_2x3x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x3x2, self).__init__()
        self.fc1=nn.Linear(2,3)
        self.relu1=nn.LeakyReLU(negative_slope=0.1)
        self.fc2=nn.Linear(3,2)
        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.Linear)):
                nn.init.orthogonal_(m.weight)

    def forward(self, x):
        x = self.fc1(x)
        h= self.relu1(x)
        x = self.fc2(h)
        
        return h,x

class MLP_2x5x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x5x2, self).__init__()
        self.fc1=nn.Linear(2,5)
        self.relu1=DualReLU()
        self.fc2=nn.Linear(5,2)

    def forward(self, x):
        x = self.fc1(x)
        h,h0,h1 = self.relu1(x)
        x = self.fc2(h)
        return h0,x
    
class MLP_2x3x3x2(nn.Module):
    
    def __init__(self):
        super(MLP_2x3x3x2, self).__init__()
        self.fc1=nn.Linear(2,3)
        self.relu1=DualReLU()
        self.fc2=nn.Linear(3,3)
        self.relu2=DualReLU()
        self.fc3=nn.Linear(3,2)

    def forward(self, x):
        x = self.fc1(x)
        h1,h10,h11 = self.relu1(x)
        x = self.fc2(h1)
        h2,h20,h21 = self.relu2(x)
        x = self.fc3(h2)
        return [h10,h20],x
    

def train(model:nn.Module,data_path,model_path=None):
    
    dm=DataManager(data_path)
   
    #model.cuda()
    cost = torch.nn.CrossEntropyLoss()#.cuda()
    optimizer = optim.SGD(model.parameters(), lr=0.03, momentum=0.9)
    batch_size=200
    
    for k,v in model.named_parameters():
        print(k,v)
    
    for epoch in range(1000):
        model.train()
        
        for batch_index in range(10):
            data,label=dm.next_batch_train(batch_size=batch_size)
            data, target = torch.FloatTensor(data), torch.LongTensor(label)
            #data, target = data.cuda(), target.cuda()
            data, target = Variable(data), Variable(target)
            optimizer.zero_grad()
            _,output = model(data)
            pred = output.data.max(1)[1]
            correct = pred.eq(target.data).cpu().sum()
            loss = cost(output, target)
            loss.backward()
            optimizer.step()
            print(str(epoch*5+batch_index),'train loss: {:.4f}, accuracy: {}/{} ({:.3f}%)'.format(loss.data.item(),correct,batch_size,100.*correct/batch_size))

        if model_path is not None:   
            torch.save(model.state_dict(),model_path)
            print("save:",model_path)

def infer(data,model_path):
    if 'MLP_2x3x2' in model_path:
        model = MLP_2x3x2()
    elif 'MLP_2x5x2' in model_path:
        model = MLP_2x5x2()
    elif 'MLP_2x3x3x2' in model_path:
        model = MLP_2x3x3x2()
    else:
        print("no model found.")
        return None,None,None
    assert os.path.exists(model_path),"%s not exist."%(model_path)
    
    sd=torch.load(model_path)
    model.load_state_dict(sd)
    model.eval()
    data = Variable(torch.FloatTensor(data))
    h,output = model(data)
    if isinstance(h,list):
        h = [x.data.cpu().numpy() for x in h]
    else:
        h = h.data.cpu().numpy()
    y = output.data.cpu().numpy()
    return h,y,np.argmax(y,axis=1)

        
    

if __name__=="__main__":
    #train(MLP_2x2x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x2x2_dual_relu.pth")
    train(MLP_2x3x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x3x2_dual_relu.pth") 
    #train(MLP_2x5x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x5x2_dual_relu.pth") 
    #train(MLP_2x3x3x2(),'../dataset/2d_data_nc_2_circle.txt',"../model/MLP_2x3x3x2_dual_relu.pth")  
       

