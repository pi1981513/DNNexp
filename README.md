# DNNexp

#### Description
DNN model explanation

#### Dependencies

See requirements.txt 

#### Run

The models were uploaded to this repo, run the command to view FC model node and decision hyperplanes:

`python3 view_fc.py`

view MLP model node hyperplanes and decision boundaries:

`python3 view_mlp.py`

view MLP model boundaries of sub-regions that seperated by hidden nodes:

`python3 view_mlp_hidden.py`

view decision tree model boundaries to compare the differences:

`python3 view_decision_tree.py`

You can re-train the model to validate the explanation:

`python3 train_fc.py` or `python3 train_mlp.py` 

#### Blog

1. [理解神经网络的数学原理（一） 全连接模型的空间划分与编码逻辑](https://blog.csdn.net/u010165147/article/details/128507539)

2. [理解神经网络的数学原理（二） 多层感知机（MLP）的空间划分与编码逻辑](https://blog.csdn.net/u010165147/article/details/128826516)

#### Reference
1. [On the Number of Linear Regions of Deep Neural Networks](https://arxiv.org/abs/1402.1869)

2. [On the number of response regions of deep feed forward networks with piece- wise linear activations](https://www.researchgate.net/publication/259400019_On_the_number_of_response_regions_of_deep_feed_forward_networks_with_piece-wise_linear_activations)

3. [On the Expressive Power of Deep Neural Networks](https://dl.acm.org/doi/10.5555/3305890.3305975)

4. [On the Number of Linear Regions of Convolutional Neural Networks](https://arxiv.org/abs/2006.00978)

5. [Bounding and Counting Linear Regions of Deep Neural Networks](https://arxiv.org/abs/1711.02114)

6. [Facing up to arrangements: face-count formulas for partitions of space by hyperplanes](https://searchworks.stanford.edu/view/684018)

7. [An Introduction to Hyperplane Arrangements](https://www.cis.upenn.edu/~cis6100/sp06stanley.pdf)

8. [Combinatorial Theory: Hyperplane Arrangements](https://ocw.mit.edu/courses/18-315-combinatorial-theory-hyperplane-arrangements-fall-2004/pages/lecture-notes/)

9. [Partern Recognition and Machine Learning](https://www.semanticscholar.org/paper/Pattern-Recognition-and-Machine-Learning-Bishop-Nasrabadi/668b1277fbece28c4841eeab1c97e4ebd0079700)

10.[Scikit-Learn Decision Trees](https://scikit-learn.org/stable/modules/tree.html)
